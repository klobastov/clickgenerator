<?php
declare(strict_types=1);

namespace Cilex\Model\Bridge;

abstract class Service
{
    protected $implementation;

    public function __construct(FormatterInterface $formatter)
    {
        $this->implementation = $formatter;
    }

    public function setImplementation(FormatterInterface $formatter): void
    {
        $this->implementation = $formatter;
    }

    abstract public function getClick();
}