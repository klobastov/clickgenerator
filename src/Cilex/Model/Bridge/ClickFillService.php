<?php
declare(strict_types=1);

namespace Cilex\Model\Bridge;

use Cilex\DataMapper\DataMapper;
use Cilex\DataMapper\DataMapperInterface;
use Cilex\DataMapper\Exception\MappingInvalid;
use Cilex\DataMapper\MemoryStorage;
use Cilex\Model\Click;
use Cilex\Scheme\Mapping\MappedSchemes;

class ClickFillService extends Service
{
    /**
     * @return DataMapperInterface|Click
     * @throws MappingInvalid
     */
    public function getClick(): DataMapperInterface
    {
        $array = $this->implementation->format();

        $clickStorage = new MemoryStorage([MappedSchemes::CLICK => $array]);
        $clickMapper = new DataMapper($clickStorage);

        $click = $clickMapper->get(MappedSchemes::CLICK);

        if (!$this->isSuccessMapped($click, $array)) {
            throw new MappingInvalid('Cant fill scheme, unknown field in: ' . get_class($click));
        }

        return $click;
    }

    private function isSuccessMapped(DataMapperInterface $scheme, array $data): bool
    {
        $keys = array_keys($data);

        foreach ($keys as $key) {
            if (!in_array($key, $scheme->getFields(), true)) {
                return false;
            }
        }

        return true;
    }
}