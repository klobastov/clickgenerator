<?php
declare(strict_types=1);

namespace Cilex\Model\Bridge;

use Cilex\Rule\CellCollection;
use InvalidArgumentException;

class WordFormatter implements FormatterInterface
{
    private $word;

    public function __construct($raw)
    {
        if (!$raw instanceof CellCollection) {
            throw new InvalidArgumentException('WordFormatter argument should be instance of "Cilex\Rule\CellCollection"');
        }

        $this->word = $raw;
    }

    public function format(): array
    {
        return $this->word->getValues();
    }
}