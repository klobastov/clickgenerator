<?php
declare(strict_types=1);

namespace Cilex\Model;

use Cilex\Scheme\Mapping\MappedScheme;

class Click extends MappedScheme
{
    private $id;

    public $ip;
    public $provider;
    public $browser;
    public $device;
    public $country;
    public $language;

    private function __construct($id, $ip, $provider, $browser, $device, $country, $language)
    {
        $this->id = $id;
        $this->ip = $ip;
        $this->provider = $provider;
        $this->browser = $browser;
        $this->device = $device;
        $this->country = $country;
        $this->language = $language;
    }

    public static function fromState(array $state): MappedScheme
    {
        $object = new self(
            self::restore('id', $state),
            self::restore('ip', $state),
            self::restore('provider', $state),
            self::restore('browser', $state),
            self::restore('device', $state),
            self::restore('country', $state),
            self::restore('language', $state)
        );

        $object->initFields(get_object_vars($object));

        return $object;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}