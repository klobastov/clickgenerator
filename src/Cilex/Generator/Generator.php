<?php
declare(strict_types=1);

namespace Cilex\Generator;

abstract class Generator implements GeneratorInterface
{
    protected $condition;

    public function __construct($condition = null)
    {
        $this->condition = $condition;
    }
}