<?php
declare(strict_types=1);

namespace Cilex\Generator;

class ProviderGenerator extends Generator
{
    public function get(array $extra = [])
    {
        $key = array_rand($this->condition);
        if (null !== $this->condition[$key]) {
            return $this->condition[$key];
        }

        return null;
    }
}