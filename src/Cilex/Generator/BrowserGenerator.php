<?php
declare(strict_types=1);

namespace Cilex\Generator;


class BrowserGenerator extends Generator
{
    public function get(array $extra = [])
    {
        $result = [];

        $key = array_rand($this->condition);

        $result['name'] = $key;
        if (null !== $this->condition[$key]) {
            $result['version'] = $this->condition[$key];
        }

        return $result;
    }
}