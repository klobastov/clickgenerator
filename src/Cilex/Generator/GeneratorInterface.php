<?php
declare(strict_types=1);

namespace Cilex\Generator;

interface GeneratorInterface
{
    public function get(array $extra = []);
}