<?php
declare(strict_types=1);

namespace Cilex\Generator;

class IpGenerator extends Generator
{
    public function get(array $extra = [])
    {
        $key = array_rand($this->condition);
        if (null !== $this->condition[$key]) {
            $mask = $this->condition[$key];

            if (false !== $long = ip2long($mask)) {
                return $mask;
            }

            $range = $this->getIPRangeByCIDR($mask);

            return long2ip(random_int(ip2long($range[0]),ip2long($range[1])));
        }

        return null;
    }

    public function getIPRangeByCIDR($cidr)
    {
        // Making sure IPs are valid
        $ipdata = explode('/', ltrim($cidr, '0'));
        $dotcount = substr_count($ipdata[0], '.');
        if ($dotcount != 3) {
            $ipdata[0] .= str_repeat('.0', (3 - $dotcount));
        }
        $cidr_address = sprintf('%s/%s', $ipdata[0], $ipdata[1]);

        // By 'unknown' http://www.php.net/manual/en/ref.network.php#30910
        $first = substr($cidr_address, 0, strpos($cidr_address, "/"));
        $netmask = substr(strstr($cidr_address, "/"), 1);

        $first_bin = str_pad(decbin(ip2long((string)$first)), 32, "0", STR_PAD_LEFT);
        $last_bin = '';
        $netmask_bin = str_pad(str_repeat("1", (integer)$netmask), 32, "0", STR_PAD_RIGHT);

        for ($i = 0; $i < 32; $i++) {
            if ($netmask_bin[$i] == "1") {
                $last_bin .= $first_bin[$i];
            } else {
                $last_bin .= "1";
            }
        }

        $last = long2ip(bindec($last_bin));

        return [$first, $last];
    }
}