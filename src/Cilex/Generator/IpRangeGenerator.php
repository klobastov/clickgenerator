<?php
declare(strict_types=1);

namespace Cilex\Generator;

use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Condition\IpRangeCondition;

class IpRangeGenerator extends Generator
{
    //@todo add providers for this type generating
    const PATTERN_RUSSIA_ROSTELECOM = [
        '2.[60-63].[0-255].[0-255]',
        '5.[136-143].[0-255].[0-255]',
        '5.228.[0-255].[0-255]',
        '31.23.[0-255].[0-255]',
        '31.28.[192-223].[0-255]',
        '31.[162-163].[0-255].[0-255]'
    ];

    const PATTERN_RUSSIA_TOMTEL = [
        '5.77.[0-31].[0-255]',
        '46.236.[128-191].[0-255]'
    ];

    private static function getRandomPattern($patterns): string
    {
        return $patterns[array_rand($patterns, 1)];
    }

    public function get(array $extra = [])
    {
        $patterns = $this->getPatterns();

        $segments = $this->getSegments($patterns);
        $pattern = $this->getAllowedPattern($segments);

        return $this->createIp($pattern);
    }


    private function getPatterns(): array
    {
        $patterns = [];

        if(!is_array($this->condition)){
            throw new ConditionInvalid('Condition should be array.');
        }

        foreach ($this->condition as $option => $value) {
            if (IpRangeCondition::OPTION_RUSSIA === $option && $value) {
                $patterns[] = self::getRandomPattern(
                    array_merge(
                        self::PATTERN_RUSSIA_TOMTEL,
                        self::PATTERN_RUSSIA_ROSTELECOM
                    )
                );
            } elseif (IpRangeCondition::OPTION_TOMTEL === $option && $value) {
                $patterns[] = self::getRandomPattern(self::PATTERN_RUSSIA_TOMTEL);
            } elseif (IpRangeCondition::OPTION_ROSTELECOM === $option && $value) {
                $patterns[] = self::getRandomPattern(self::PATTERN_RUSSIA_ROSTELECOM);
            } elseif (IpRangeCondition::OPTION_PATTERN === $option && null !== $value && is_string($value)) {
                $patterns[] = $value;
            }
        }

        if (empty($patterns)) {
            throw new ConditionInvalid('Patterns list is empty. Use option to fill patterns.');
        }
        return $patterns;
    }

    private function getSegments(array $masks): array
    {
        $segments_per_mask = [];
        foreach ($masks as $mask) {
            $segments = explode('.', $mask);
            if (4 !== count($segments)) {
                throw new ConditionInvalid('Ip pattern should contain 4 segments.');
            }
            $segments_per_mask[] = $segments;
        }
        return $segments_per_mask;
    }

    private function getSegmentNumber($pattern, $length): int
    {
        return substr_count($pattern, '.', 0, $length);
    }

    private function getRange(string $pattern): array
    {
        $range = [];

        $count = preg_match_all(
            '/\[(?\'from\'[\d]+)[\-](?\'to\'[\d]+)\]/',
            $pattern,
            $matches,
            PREG_OFFSET_CAPTURE
        );

        for ($i = 0; $i < $count; $i++) {
            $segment_from = $this->getSegmentNumber($pattern, $matches['from'][$i][1]);
            $segment_to = $this->getSegmentNumber($pattern, $matches['to'][$i][1]);

            $range[$segment_from]['from'] = (int)$matches['from'][$i][0];
            $range[$segment_to]['to'] = (int)$matches['to'][$i][0];
        }

        return $range;
    }

    private function getAllowedPattern(array $segments): array
    {
        $static_segments = [];
        if (1 === count($segments)) {
            for ($s = 0; $s < 4; $s++) {
                if (is_numeric($segments[0][$s])) {
                    $static_segments[] = (int)$segments[0][$s];
                } else {
                    $static_segments[] = $segments[0][$s];
                }
            }
            return $static_segments;
        }

        for ($m = 0, $iMax = count($segments); $m < $iMax; $m++) {
            if (!isset($segments[$m + 1])) {
                continue;
            }
            for ($s = 0; $s < 4; $s++) {
                $segment_a = is_numeric($segments[$m][$s]);
                $segment_b = is_numeric($segments[$m + 1][$s]);
                if ($segment_a || $segment_b) {
                    if ($segment_a && !$segment_b) {
                        $range = $this->getRange($segments[$m + 1][$s]);
                        if ($segments[$m][$s] <= $range[0]['to'] && $segments[$m][$s] >= $range[0]['from']) {
                            $static_segments[] = (int)$segments[$m][$s];
                        } else {
                            throw new ConditionInvalid("[\]Static segment: {$segments[$m][$s]}, conflict with range {$range[0]['from']} - {$range[0]['to']}");
                        }
                    } elseif ($segment_b && !$segment_a) {
                        $range = $this->getRange($segments[$m][$s]);
                        if ($segments[$m + 1][$s] <= $range[0]['to'] && $segments[$m + 1][$s] >= $range[0]['from']) {
                            $static_segments[] = (int)$segments[$m + 1][$s];
                        } else {
                            throw new ConditionInvalid("[/]Static segment: {$segments[$m+1][$s]}, conflict with range {$range[0]['from']} - {$range[0]['to']}");
                        }
                    } elseif ($segments[$m][$s] === $segments[$m + 1][$s]) {
                        $static_segments[] = (int)$segments[$m][$s];
                    } else {
                        throw new ConditionInvalid("Conflict static segments: {$segments[$m][$s]} and {$segments[$m+1][$s]}");
                    }
                } elseif (!$segment_a && !$segment_b) {
                    $range_a = $this->getRange($segments[$m + 1][$s]);
                    $range_b = $this->getRange($segments[$m][$s]);
                    $safe_range = [];
                    if ($range_a[0]['from'] < $range_b[0]['from']) {
                        $safe_range[] = $range_b[0]['from'];
                    } else {
                        $safe_range[] = $range_a[0]['from'];
                    }
                    if ($range_a[0]['to'] > $range_b[0]['to']) {
                        $safe_range[] = $range_b[0]['to'];
                    } else {
                        $safe_range[] = $range_a[0]['to'];
                    }
                    $static_segments[] = '[' . implode('-', $safe_range) . ']';
                }
            }
        }

        return $static_segments;
    }

    private function createIp($pattern): string
    {
        $ip = [];
        for ($s = 0; $s < 4; $s++) {
            if (is_numeric($pattern[$s])) {
                $ip[] = $pattern[$s];
            } else {
                $range = $this->getRange($pattern[$s]);
                $ip[] = random_int($range[0]['from'], $range[0]['to']);
            }
        }
        return implode('.', $ip);
    }
}