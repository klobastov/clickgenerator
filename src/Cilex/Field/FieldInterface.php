<?php
declare(strict_types=1);

namespace Cilex\Field;

use Cilex\Scheme\Scheme;

interface FieldInterface
{
    public function __construct(string $field, Scheme $scheme);

    public function getField(): string;

    public function getScheme(): Scheme;
}