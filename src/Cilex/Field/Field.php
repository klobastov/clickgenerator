<?php
declare(strict_types=1);

namespace Cilex\Field;

use BadFunctionCallException;
use Cilex\Scheme\Scheme;
use Cilex\Scheme\Exception\FieldNotExist;

abstract class Field implements FieldInterface
{
    private $field;
    private $scheme;

    /**
     * Field constructor.
     * @param string $field
     * @param Scheme $scheme
     * @param array $extra
     * @throws FieldNotExist
     * @throws \BadFunctionCallException
     */
    public function __construct(string $field, Scheme $scheme, array $extra = [])
    {
        if (!in_array($field, $scheme->getFields(), true)) {
            throw new FieldNotExist($field);
        }

        if (!empty($extra)) {
            throw new BadFunctionCallException('Extra argument not used in field.');
        }

        $this->field = $field;
        $this->scheme = $scheme;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return Scheme
     */
    public function getScheme(): Scheme
    {
        return $this->scheme;
    }
}