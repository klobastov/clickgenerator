<?php
declare(strict_types=1);

namespace Cilex\Api;

use Cilex\Api\Exception\RequestInvalid;
use Requests;

class APIRequest
{
    private $request;

    public function __construct(string $url)
    {
        $headers = ['Accept' => 'application/json'];
        $options = [];

        $this->request = Requests::get($url, $headers, $options);
    }

    public function getResponse(): string
    {
        if (200 !== $this->request->status_code) {
            throw new RequestInvalid("Request api invalid, code: {$this->request->status_code}");
        }

        return $this->request->body;
    }
}