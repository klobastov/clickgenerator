<?php
declare(strict_types=1);

namespace Cilex\Api\Exception;

use Exception;
use Throwable;

class RequestInvalid extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Invalid request';
        }
        parent::__construct($message, $code, $previous);
    }
}