<?php
declare(strict_types=1);

namespace Cilex\Factory;

use Cilex\Condition\Cell;
use Cilex\Condition\Condition;
use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Factory\Exception\FactoryOptionInvalid;
use Cilex\Factory\Exception\FactoryOptionNotFound;
use Cilex\Rule\AbstractConditionFactory;
use Cilex\Rule\CellCollection;
use Cilex\Rule\ConditionCollection;
use Closure;

class WordFactory extends AbstractConditionFactory
{
    const OPTION_CELLS = 'conditions';
    const OPTION_CELL_KEYS = 'names';
    const OPTION_CHAINED_CELLS = 'chains';

    /**
     * @see WordFactory::createCondition()
     * @param array $options
     * @return CellCollection|ConditionCollection
     */
    public function createWord(array $options): CellCollection
    {
        return $this->createCondition($options);
    }

    public function createCondition(array $options): ConditionCollection
    {
        if (!isset($options[self::OPTION_CELLS])) {
            throw new FactoryOptionNotFound(self::OPTION_CELLS);
        }

        $conditions = $options[self::OPTION_CELLS];
        if (!is_array($conditions)) {
            throw new FactoryOptionInvalid(self::OPTION_CELLS);
        }

        if (isset($options[self::OPTION_CELL_KEYS]) && !empty($options[self::OPTION_CELL_KEYS])) {
            $names = array_reverse($options[self::OPTION_CELL_KEYS]);
        }

        if (isset($options[self::OPTION_CHAINED_CELLS])) {
            $chains = $options[self::OPTION_CHAINED_CELLS];
        }

        $cellFactory = new CellFactory();
        $cells = [];

        /**
         * @var $conditions array
         */
        foreach ($conditions as $key => $condition) {
            if ($condition instanceof Closure) {
                $condition = $condition();
            }

            if (!$condition instanceof Condition) {
                throw new ConditionInvalid('Word factory, invalid condition.');
            }

            $name = $key;
            if (isset($names)) {
                $name = array_pop($names);
            }
            $cells[$name] = $cellFactory->createSimpleCell([CellFactory::OPTION_CONDITION => $condition]);
        }

        if (!isset($chains) || !is_array($chains)) {
            return new CellCollection($cells);
        }

        foreach ($chains as $chain => $dependenciesNames) {
            if (!is_array($dependenciesNames) || !isset($cells[$chain])) {
                continue;
            }

            /**
             * @var $cell Cell
             */
            $cell = $cells[$chain];

            $dependencies = [];
            foreach ($dependenciesNames as $dependenciesName) {
                $dependencies[$dependenciesName] = $cells[$dependenciesName];
            }

            $cells[$chain] = $cellFactory->createSimpleCell([
                CellFactory::OPTION_CONDITION => $cell->getCondition(),
                CellFactory::OPTION_DEPENDENCIES => $dependencies
            ]);
        }

        return new CellCollection($cells);
    }

}