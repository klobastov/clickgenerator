<?php
declare(strict_types=1);

namespace Cilex\Factory\Exception;

use Exception;
use Throwable;

class FactoryOptionInvalid extends Exception
{
    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        $message = "Invalid option type: '{$message}', was sent in factory.";
        parent::__construct($message, $code, $previous);
    }
}