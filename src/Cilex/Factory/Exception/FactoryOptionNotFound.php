<?php
declare(strict_types=1);

namespace Cilex\Factory\Exception;

use Exception;
use Throwable;

class FactoryOptionNotFound extends Exception
{
    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        $message = "Not exist factory options: {$message}";
        parent::__construct($message, $code, $previous);
    }
}