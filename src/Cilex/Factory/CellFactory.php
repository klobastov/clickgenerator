<?php
declare(strict_types=1);

namespace Cilex\Factory;

use Cilex\Condition\Cell;
use Cilex\Condition\Condition;
use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Factory\Exception\FactoryOptionNotFound;

class CellFactory
{
    const OPTION_CONDITION = 'condition';
    const OPTION_DEPENDENCIES = 'dependencies';

    public function createSimpleCell(array $options): Cell
    {
        if (!isset($options[self::OPTION_CONDITION])) {
            throw new FactoryOptionNotFound(self::OPTION_CONDITION);
        }

        $condition = $options[self::OPTION_CONDITION];

        if (!$condition instanceof Condition) {
            throw new ConditionInvalid('Cell factory, invalid condition.');
        }

        $dependencies = [];
        if (isset($options[self::OPTION_DEPENDENCIES])) {
            $dependencies = $options[self::OPTION_DEPENDENCIES];
        }

        return new Cell($condition, $dependencies);
    }
}