<?php
declare(strict_types=1);

namespace Cilex\Rule;

abstract class ConditionCollection
{
    abstract public function getConditions(): array;
    abstract public function getValues(): array;
}