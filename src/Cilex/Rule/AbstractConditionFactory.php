<?php
declare(strict_types=1);

namespace Cilex\Rule;

abstract class AbstractConditionFactory
{
    abstract public function createCondition(array $options): ConditionCollection;
}