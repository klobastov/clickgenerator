<?php
declare(strict_types=1);

namespace Cilex\Rule;

use Cilex\Condition\Cell;

class CellCollection extends ConditionCollection
{
    private $cells;

    public function __construct(array $cells)
    {
        $this->cells = $cells;

        $this->validate();
    }

    protected function validate(): bool
    {
        foreach ($this->cells as $cell) {
            if (!$cell instanceof Cell) {
                return false;
            }
        }
        return true;
    }

    public function getConditions(): array
    {
        $conditions = [];

        /**
         * @var $cell Cell
         */
        foreach ($this->cells as $cell) {
            $conditions[] = $cell
                ->getCondition()
                ->get();
        }
        return $conditions;
    }

    public function getValues(): array
    {
        $values = [];

        /**
         * @var $cell Cell
         */
        foreach ($this->cells as $key => $cell) {
            if (!$cell->hasDependencies()) {
                $values[$key] = $cell->getCondition()->generator()->get();
            } else {
                $dependencies = $cell->getDependencies();
                /**
                 * @var $dependencyCell Cell
                 */
                $used = [];
                foreach ($dependencies as $dependency => $dependencyCell) {
                    $value = $dependencyCell->getCondition()->generator()->get();
                    $used[$dependency] = $value;
                    $values[$dependency] = $value;
                }
                $values[$key] = $cell->getCondition()->generator()->get(['used' => $used]);
            }
        }
        return $values;
    }

    //@todo если пригодится, надо доработать
    public function getValue()
    {
        $all = $this->getValues();
        if (1 === $this->getSize()) {
            return array_shift($all);
        }
        return null;
    }

    private function getSize()
    {
        return count($this->cells);
    }
}