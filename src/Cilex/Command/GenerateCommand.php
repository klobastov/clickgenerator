<?php
declare(strict_types=1);

namespace Cilex\Command;

use Cilex\Api\APIRequest;
use Cilex\Condition\BrowserCondition;
use Cilex\Condition\CountryCondition;
use Cilex\Condition\DeviceCondition;
use Cilex\Condition\IpCondition;
use Cilex\Condition\LanguageCondition;
use Cilex\Condition\ProviderCondition;
use Cilex\DataMapper\ConditionMapper;
use Cilex\DataMapper\ConditionsStorage;
use Cilex\Factory\WordFactory;
use Cilex\Model\Bridge\ClickFillService;
use Cilex\Model\Bridge\WordFormatter;
use Cilex\Provider\Console\Command;
use Cilex\Repository\ClickRepository;
use Cilex\Repository\SQLiteStorage;
use Cilex\Scheme\CampaignScheme;
use Cilex\Scheme\Mapping\Bridge\JsonFormatter;
use Cilex\Scheme\Mapping\Bridge\SchemeContentService;
use Cilex\Scheme\Mapping\MappedSchemes;
use InvalidArgumentException;
use NoSQLite\NoSQLite;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCommand extends Command
{
    const ARGUMENT_CAMPAIGN = 'campaignid';

    protected function configure(): void
    {
        $this
            ->setName('generator:generate')
            ->addArgument(self::ARGUMENT_CAMPAIGN, InputArgument::REQUIRED, 'ID кампании')
            ->setDescription('Запуск генерации кликов');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $this->getId($input);

        $request = new APIRequest('http://symfony.dev/v1/campaigns/' . $id);

        $contentService = new SchemeContentService(CampaignScheme::class, new JsonFormatter($request->getResponse()));

        $conditionsStorage = new ConditionsStorage($contentService->getContent());
        $conditionMapper = new ConditionMapper($conditionsStorage);

        $factory = new WordFactory();
        $word = $factory->createWord([
            WordFactory::OPTION_CELLS => [
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_BROWSER);
                    $browsers = BrowserCondition::fromApiState($scheme->condition);
                    return new BrowserCondition($browsers);
                },
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_DEVICE);
                    $devices = DeviceCondition::fromApiState($scheme->condition);
                    return new DeviceCondition($devices);
                },
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_COUNTY);
                    $countries = CountryCondition::fromApiState($scheme->condition);
                    return new CountryCondition($countries);
                },
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_LANGUAGE);
                    $languages = LanguageCondition::fromApiState($scheme->condition);
                    return new LanguageCondition($languages);
                },
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_IP_ADDRESS);
                    $languages = IpCondition::fromApiState($scheme->condition);
                    return new IpCondition($languages);
                },
                function () use ($conditionMapper) {
                    $scheme = $conditionMapper->get(MappedSchemes::CONDITION, ConditionsStorage::TYPE_PROVIDER);
                    $languages = ProviderCondition::fromApiState($scheme->condition);
                    return new ProviderCondition($languages);
                }
            ],
            WordFactory::OPTION_CELL_KEYS => [
                'browser',
                'device',
                'country',
                'language',
                'ip',
                'provider'
            ]
        ]);

        $fillService = new ClickFillService(new WordFormatter($word));

        $driver = new NoSQLite('storage.sqlite');
        $reserveRepository = new ClickRepository(new SQLiteStorage($driver, "campaign{$id}"));

        $checkTime = 10;
        $starttime = microtime(true);
        while (1) {
            sleep(1);
            $current = (int)date('s', (int)(microtime(true) - $starttime));
            if ($current >= $checkTime) {
                $size = $reserveRepository->getSize();
                if ($size < 10000) {
                    for ($i = 1; $i <= 500; $i++) {
                        $reserveRepository->save($fillService->getClick());
                    }
                }
                $starttime = microtime(true);
            }

        }
    }

    private function getId(InputInterface $input): int
    {
        $raw = $input->getArgument(self::ARGUMENT_CAMPAIGN);

        if (!is_numeric($raw)) {
            throw new InvalidArgumentException(self::ARGUMENT_CAMPAIGN . 'should be numeric');
        }

        return (int)$raw;
    }
}