<?php
declare(strict_types=1);

namespace Cilex\Command;

use Cilex\Provider\Console\Command;
use Cilex\Repository\ClickRepository;
use Cilex\Repository\ClickStackRepository;
use Cilex\Repository\RedirectAsyncStorage;
use Cilex\Repository\SQLiteStorage;
use InvalidArgumentException;
use NoSQLite\NoSQLite;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RealTimeCommand extends Command
{
    const ARGUMENT_CAMPAIGN = 'campaignid';
    const ARGUMENT_PERIOD = 'period';
    const OPTION_AMPLITUDE = 'amplitude';
    const OPTION_ABOVE_ZERO = 'abovezero';

    protected function configure(): void
    {
        $this
            ->setName('sender:sendrtsin')
            ->addArgument(
                self::ARGUMENT_CAMPAIGN,
                InputArgument::REQUIRED,
                'ID кампании'
            )
            ->addArgument(
                self::ARGUMENT_PERIOD,
                InputArgument::REQUIRED,
                'Количество времени в секундах, необходимое для достижения полного периода функцией sin'
            )
            ->addOption(
                self::OPTION_AMPLITUDE,
                'a',
                InputOption::VALUE_OPTIONAL,
                'Максимально достижимая амплитуда кликов на компанию. ' .
                'Количество кликов в пике равно сумме этого параметра и параметра ' . self::OPTION_ABOVE_ZERO,
                200
            )
            ->addOption(
                self::OPTION_ABOVE_ZERO,
                'z',
                InputOption::VALUE_OPTIONAL,
                'Насколько выше нуля должны быть клики, они не могут уходить в минус, как функция sin',
                200
            )
            ->setDescription('Запуск отправки кликов в реальном времени по синусоиде.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $this->getId($input);
        $period = $this->getPeriod($input);
        $amplitude = $input->getOption(self::OPTION_AMPLITUDE);
        $aboveZero = $input->getOption(self::OPTION_ABOVE_ZERO);


        $driver = new NoSQLite('storage.sqlite');
        $reserveRepository = new ClickRepository(new SQLiteStorage($driver, "campaign{$id}"));

        $starttime = microtime(true);
        while (1) {
            sleep(1);

            $current = (int)date('s', (int)(microtime(true) - $starttime));
            if ($current >= $period) {
                $starttime = microtime(true);
            }

            $percentage = $current / ($period / 100);
            $rad = (6.28 / 100) * $percentage;
            $count = $aboveZero + $amplitude * sin($rad);

            if (1 <= $count) {
                $clicks = $reserveRepository->pop((int)$count);

                $clicksRepository = new ClickStackRepository(new RedirectAsyncStorage($id));
                $clicksRepository->save($clicks);
            }
        }
    }

    private function getId(InputInterface $input): int
    {
        $raw = $input->getArgument(self::ARGUMENT_CAMPAIGN);

        if (!is_numeric($raw)) {
            throw new InvalidArgumentException(self::ARGUMENT_CAMPAIGN . 'should be numeric');
        }

        return (int)$raw;
    }

    private function getPeriod(InputInterface $input): int
    {
        $raw = $input->getArgument(self::ARGUMENT_PERIOD);

        if (!is_numeric($raw)) {
            throw new InvalidArgumentException(self::ARGUMENT_CAMPAIGN . 'should be numeric');
        }

        return (int)$raw;
    }
}