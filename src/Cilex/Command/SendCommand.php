<?php
declare(strict_types=1);

namespace Cilex\Command;


use Cilex\Provider\Console\Command;
use Cilex\Repository\ClickRepository;
use Cilex\Repository\ClickStackRepository;
use Cilex\Repository\RedirectAsyncStorage;
use Cilex\Repository\SQLiteStorage;
use InvalidArgumentException;
use NoSQLite\NoSQLite;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCommand extends Command
{
    const ARGUMENT_CAMPAIGN = 'campaignid';
    const ARGUMENT_COUNT_CLICKS = 'countclicks';

    protected function configure(): void
    {
        $this
            ->setName('sender:send')
            ->addArgument(self::ARGUMENT_CAMPAIGN, InputArgument::REQUIRED, 'ID кампании')
            ->addArgument(self::ARGUMENT_COUNT_CLICKS, InputArgument::REQUIRED, 'Количество кликов')
            ->setDescription('Запуск отправки кликов');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $this->getId($input);
        $count = $this->getCount($input);

        $driver = new NoSQLite('storage.sqlite');
        $reserveRepository = new ClickRepository(new SQLiteStorage($driver, "campaign{$id}"));

        $clicks = $reserveRepository->pop($count);

        $clicksRepository = new ClickStackRepository(new RedirectAsyncStorage($id));
        $clicksRepository->save($clicks);
    }

    private function getId(InputInterface $input): int
    {
        $raw = $input->getArgument(self::ARGUMENT_CAMPAIGN);

        if (!is_numeric($raw)) {
            throw new InvalidArgumentException(self::ARGUMENT_CAMPAIGN . 'should be numeric');
        }

        return (int)$raw;
    }

    private function getCount(InputInterface $input): int
    {
        $raw = $input->getArgument(self::ARGUMENT_COUNT_CLICKS);

        if (!is_numeric($raw)) {
            throw new InvalidArgumentException(self::ARGUMENT_CAMPAIGN . 'should be numeric');
        }

        return (int)$raw;
    }
}