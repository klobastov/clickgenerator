<?php
declare(strict_types=1);

namespace Cilex\Repository\Exception;

use Exception;
use Throwable;

class StorageInvalid extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Invalid storage';
        }
        parent::__construct($message, $code, $previous);
    }
}