<?php
declare(strict_types=1);

namespace Cilex\Repository;

class ClickStackRepository
{
    private $persistence;

    public function __construct(StorageInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    public function save(array $stack): void
    {
        $this->persistence->persist($stack);
    }
}