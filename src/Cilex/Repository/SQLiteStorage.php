<?php
declare(strict_types=1);

namespace Cilex\Repository;

use NoSQLite\NoSQLite;

class SQLiteStorage implements StorageInterface
{
    private $driver;

    private $lastId = 0;
    private $store;

    public function __construct(NoSQLite $driver, $storage = 'storage')
    {
        $this->driver = $driver;

        $this->store = $this->driver->getStore($storage);
    }

    public function persist(array $data): int
    {
        $this->lastId++;

        $data['id'] = $this->lastId;
        $this->store->set((string)$this->lastId, json_encode($data));

        return $this->lastId;
    }

    public function retrieve(int $id): ?array
    {
        $value = $this->store->get((string)$id);
        if (null !== $value) {
            return json_decode($value, true);
        }

        return null;
    }

    public function getSize(): int
    {
        return $this->store->count();
    }

    public function delete(int $id): void
    {
        $this->store->delete((string)$id);
    }
}