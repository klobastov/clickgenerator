<?php
declare(strict_types=1);

namespace Cilex\Repository;

use Cilex\Model\Click;
use Cilex\Repository\Exception\StorageInvalid;
use InvalidArgumentException;
use Requests;

class RedirectAsyncStorage implements StorageInterface
{
    const HEADER_PREFIX = 'X-Generator-';
    const REDIRECT_HOST = 'http://symfony.dev/';
    const REDIRECT_PATH = 'v1/campaigns/';

    private $redirectURL;

    public function __construct($campaign)
    {
        if (!is_numeric($campaign)) {
            throw new InvalidArgumentException('Campaign should be numeric');
        }

        $this->redirectURL = self::REDIRECT_HOST . self::REDIRECT_PATH . $campaign;
    }

    public function persist(array $data): int
    {
        $options = [
            'blocking' => false, //async
            'timeout' => 12,
            'connect_timeout' => 10,
            'useragent' => 'GENERATOR',
            'follow_redirects' => false,
            'verifyname' => false
        ];

        $requests = [];
        foreach ($data as $click) {
            if (!$click instanceof Click) {
                throw new StorageInvalid('Cant save click, should be instance of "Cilex\Model\Click" class');
            }
            $request['url'] = $this->redirectURL;
            $request['headers'] = $this->toHeaders($click);
            $request['type'] = Requests::GET;
            $requests[] = $request;
        }

        Requests::request_multiple($requests, $options);

        return 1;
    }

    public function retrieve(int $id): ?array
    {
        throw new StorageInvalid('Method while not supported by redirect server.');
    }

    public function getSize(): int
    {
        throw new StorageInvalid('Method while not supported by redirect server.');
    }

    public function delete(int $id): void
    {
        throw new StorageInvalid('Method while not supported by redirect server.');
    }

    private function toHeaders(Click $click): array
    {
        $headers = [];

        $values = $click->getFields(false);

        foreach ($values as $field => &$value) {
            if (is_array($value)) {
                foreach ($value as $subName => $subValue) {
                    $headers[self::HEADER_PREFIX . $field . '-' . $subName] = $subValue;
                }
            } else {
                $headers[self::HEADER_PREFIX . $field] = $value;
            }

        }

        return $headers;
    }
}