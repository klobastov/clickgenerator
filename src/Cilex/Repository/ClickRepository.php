<?php
declare(strict_types=1);

namespace Cilex\Repository;

use Cilex\Model\Click;
use Cilex\Repository\Exception\StorageInvalid;
use Cilex\Scheme\Mapping\MappedScheme;
use InvalidArgumentException;

class ClickRepository
{
    private $persistence;

    public function __construct(StorageInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    /**
     * @param int $id
     * @return MappedScheme|Click
     * @throws InvalidArgumentException
     */
    public function findById(int $id): MappedScheme
    {
        $arrayData = $this->persistence->retrieve($id);

        if (null === $arrayData) {
            throw new InvalidArgumentException(sprintf('Click with ID %d does not exist', $id));
        }

        return Click::fromState($arrayData);
    }

    public function save(Click $click): void
    {
        $values = $click->getFields(false);
        $id = $this->persistence->persist($values);

        $click->setId($id);
    }

    public function delete(int $id): void
    {
        $this->persistence->delete($id);
    }

    public function pop(int $length = 1): array
    {
        $size = $this->getSize();
        if (0 === $size) {
            throw new StorageInvalid('Storage is empty.');
        }

        $stack = [];

        $length = $length === 1 ? $length + 1 : $length;
        for ($i = 0; $i < $length; $i++) {
            $id = $size - $i;

            $stack[] = $this->findById($id);
            $this->delete($id);
        }

        return $stack;
    }

    public function getSize(): int
    {
        return $this->persistence->getSize();
    }
}