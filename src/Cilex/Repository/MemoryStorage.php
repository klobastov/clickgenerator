<?php
declare(strict_types=1);

namespace Cilex\Repository;

class MemoryStorage implements StorageInterface
{
    private $data = [];
    private $lastId = 0;

    public function persist(array $data): int
    {
        $this->lastId++;

        $data['id'] = $this->lastId;
        $this->data[$this->lastId] = $data;

        return $this->lastId;
    }

    public function retrieve(int $id): ?array
    {
        if (!isset($this->data[$id])) {
            return null;
        }

        return $this->data[$id];
    }

    public function getSize(): int
    {
        return count($this->data);
    }

    public function delete(int $id): void
    {
        if (isset($this->data[$id])) {
            unset($this->data[$id]);
        }
    }
}