<?php
declare(strict_types=1);

namespace Cilex\Repository;

interface StorageInterface
{
    public function persist(array $data): int;

    public function retrieve(int $id): ?array;

    public function getSize(): int;

    public function delete(int $id): void;
}