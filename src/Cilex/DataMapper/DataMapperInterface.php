<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\SchemeInterface;

interface DataMapperInterface extends SchemeInterface
{
    public static function fromState(array $state): MappedScheme;
}