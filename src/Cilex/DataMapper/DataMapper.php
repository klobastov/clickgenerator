<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

use Cilex\DataMapper\Exception\MappingInvalid;
use Cilex\Scheme\Mapping\MappedSchemes;

class DataMapper
{
    private $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function get(int $id): DataMapperInterface
    {
        $state = $this->storage->find($id);
        if ($state === null) {
            throw new MappingInvalid("Scheme with id: {$id} not found in storage.");
        }

        return $this->map($id, $state);
    }

    private function map(int $id, array $state): DataMapperInterface
    {
        $scheme = MappedSchemes::getOne($id);
        if (!method_exists($scheme, 'fromState')) {
            throw new MappingInvalid("Cant map scheme: {$scheme}, not implemented DataMapperInterface.");
        }

        return $scheme::fromState($state);
    }
}