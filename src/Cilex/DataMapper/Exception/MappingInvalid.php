<?php
declare(strict_types=1);

namespace Cilex\DataMapper\Exception;

use Exception;
use Throwable;

class MappingInvalid extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Invalid mapping';
        }
        parent::__construct($message, $code, $previous);
    }
}