<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

trait StateTrait
{
    public static function restore(string $key, array $state, $type = null)
    {
        if (isset($state[$key])) {
            return $state[$key];
        }

        return $type;
    }
}