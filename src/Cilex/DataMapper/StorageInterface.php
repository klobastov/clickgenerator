<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

interface StorageInterface
{
    public function __construct(array $data);
    
    public function find(int $id);
}