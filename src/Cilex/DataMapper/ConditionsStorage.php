<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

use Cilex\DataMapper\Exception\MappingInvalid;
use InvalidArgumentException;

class ConditionsStorage extends MemoryStorage
{
    private $containTypes;

    const TYPE_UNIQUE_COOKIE = 10;
    const TYPE_IP_ADDRESS = 20;
    const TYPE_BROWSER = 30;
    const TYPE_DEVICE = 40;
    const TYPE_OS = 50;
    const TYPE_PROVIDER = 60;
    const TYPE_COUNTY = 70;
    const TYPE_LANGUAGE = 80;

    public function __construct(array $data)
    {
        if (!isset($data['flow']['streams']['conditions'])) {
            throw new InvalidArgumentException('Rules should contain element with key path = "[\'flow\'][\'streams\'][\'conditions\']"');
        }

        parent::__construct($this->prepare($data['flow']['streams']['conditions']));
    }

    private function prepare(array $conditions): array
    {
        $data = [];

        foreach ($conditions as $condition) {
            if (!isset(self::getTypes()[$condition['type']])) {
                throw new MappingInvalid("Unknown condition type: {$condition['type']}");
            }

            $this->containTypes[] = $condition['type'];

            $data[$condition['type']] = [
                'active' => $condition['active'],
                'type' => $condition['type'],
                'condition' => $condition['condition']['values'] ?? $condition['condition'],
            ];
        }

        return $data;
    }

    public function getContainTypes(): array
    {
        return $this->containTypes;
    }

    public static function getTypes()
    {
        return [
            static::TYPE_BROWSER => 'Browser condition',
            static::TYPE_IP_ADDRESS => 'IP address condition',
            static::TYPE_LANGUAGE => 'Language condition',
            static::TYPE_COUNTY => 'Country condition',
            static::TYPE_PROVIDER => 'Provider condition',
            static::TYPE_OS => 'OS condition',
            static::TYPE_DEVICE => 'Device condition',
            static::TYPE_UNIQUE_COOKIE => 'Unique cookie condition',
        ];
    }
}