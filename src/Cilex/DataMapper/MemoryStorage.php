<?php
declare(strict_types=1);

namespace Cilex\DataMapper;

class MemoryStorage implements StorageInterface
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function find(int $id)
    {
        if (!isset($this->data[$id])) {
            return null;
        }

        return $this->data[$id];
    }

    public function getSize(): int
    {
        return count($this->data);
    }
}