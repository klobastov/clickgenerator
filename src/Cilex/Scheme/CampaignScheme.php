<?php
declare(strict_types=1);

namespace Cilex\Scheme;

use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\Nesting\LayerNestingScheme;

class CampaignScheme extends LayerNestingScheme
{
    public $id;
    public $flow;

    private function __construct($id, $flow)
    {
        $this->id = $id;
        $this->flow = $flow;
    }

    public static function fromState(array $state): MappedScheme
    {
        $object = new self(
            self::restore('id', $state),
            self::restore('flow', $state)
        );

        $object->initFields(get_object_vars($object));

        return $object;
    }

    public function getLayer(): int
    {
        return 1;
    }
}