<?php
declare(strict_types=1);

namespace Cilex\Scheme;

use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\Nesting\LayerNestingScheme;

class FlowScheme extends LayerNestingScheme
{
    public $id;
    public $streams = [];

    private function __construct($id, $stream)
    {
        $this->id = $id;
        $this->streams = $stream;
    }

    public static function fromState(array $state): MappedScheme
    {
        $object = new self(
            self::restore('id', $state),
            self::restore('streams', $state, [])
        );

        $object->initFields(get_object_vars($object));

        return $object;
    }

    public function getLayer(): int
    {
        return 2;
    }
}