<?php
declare(strict_types=1);

namespace Cilex\Scheme;

interface SchemeInterface
{
    public function getFields(): array;
}