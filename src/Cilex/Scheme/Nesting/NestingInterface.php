<?php
declare(strict_types=1);

namespace Cilex\Scheme\Nesting;

interface NestingInterface
{
    public function getParent();

    public function getChildren(): array;
}