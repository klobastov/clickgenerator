<?php
declare(strict_types=1);

namespace Cilex\Scheme\Nesting;

interface LayerNestingInterface extends NestingInterface
{
    public function getLayer(): int;
}