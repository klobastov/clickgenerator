<?php
declare(strict_types=1);

namespace Cilex\Scheme\Nesting;

use Cilex\DataMapper\DataMapperInterface;
use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\Mapping\MappedSchemes;

abstract class LayerNestingScheme extends MappedScheme implements LayerNestingInterface
{
    private $layers = [];

    private function initLayerNesting(): void
    {
        $schemes = MappedSchemes::get();

        foreach ($schemes as $id => $scheme) {
            $object = $scheme::fromState([]);
            if (!$object instanceof DataMapperInterface) {
                continue;
            }

            if ($object instanceof LayerNestingInterface) {
                $this->layers[$id] = $object->getLayer();
            }
        }
    }

    public function getParent(): ?LayerNestingScheme
    {
        $this->initLayerNesting();

        if (false !== $id = array_search($this->getLayer() - 1, $this->layers, true)) {
            $scheme = MappedSchemes::getOne($id);
            $object = $scheme::fromState([]);
            if ($object instanceof DataMapperInterface) {
                return $object;
            }
        }

        return null;
    }

    public function getChildren(): array
    {
        $this->initLayerNesting();
        $children = [];
        foreach ($this->layers as $id => $layer) {
            if ($layer === $this->getLayer() + 1) {
                $scheme = MappedSchemes::getOne($id);
                $object = $scheme::fromState([]);
                if ($object instanceof DataMapperInterface) {
                    $children[] = $object;
                }
            }
        }

        return $children;
    }
}