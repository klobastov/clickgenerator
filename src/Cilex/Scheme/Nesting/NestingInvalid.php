<?php
declare(strict_types=1);

namespace Cilex\Scheme\Nesting;

use Exception;
use Throwable;

class NestingInvalid extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Invalid nesting';
        }
        parent::__construct($message, $code, $previous);
    }
}