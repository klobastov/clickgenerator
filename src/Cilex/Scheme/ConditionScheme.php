<?php
declare(strict_types=1);

namespace Cilex\Scheme;


use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\Nesting\LayerNestingScheme;

class ConditionScheme extends LayerNestingScheme
{
    public $active;
    public $type;
    public $condition;

    private function __construct($active, $type, $condition)
    {
        $this->active = $active;
        $this->type = $type;
        $this->condition = $condition;
    }

    public static function fromState(array $state): MappedScheme
    {
        $object = new self(
            self::restore('active', $state),
            self::restore('type', $state),
            self::restore('condition', $state)
        );

        $object->initFields(get_object_vars($object));

        return $object;
    }

    public function getLayer(): int
    {
        return 4;
    }
}