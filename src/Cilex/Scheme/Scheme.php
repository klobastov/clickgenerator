<?php
declare(strict_types=1);

namespace Cilex\Scheme;

use Cilex\Scheme\Exception\FieldNotAllowed;
use Cilex\Scheme\Exception\FieldNotEnabled;
use Cilex\Scheme\Exception\FieldNotExist;

abstract class Scheme implements SchemeInterface
{
    private $fields = [];

    /**
     * @param $name
     * @throws FieldNotAllowed
     * @throws FieldNotEnabled
     * @throws FieldNotExist
     */
    public function __get($name)
    {
        if (!property_exists($this, $name)) {
            throw new FieldNotExist($name);
        }

        if (!isset($this->$name)) {
            throw new FieldNotAllowed($name);
        }

        if (!in_array($name, $this->getFields(), true)) {
            throw new FieldNotEnabled($name);
        }

        $method = sprintf('get%s', ucfirst($name));
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }

        return $this->$name;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     * @throws FieldNotAllowed
     * @throws FieldNotEnabled
     * @throws FieldNotExist
     */
    public function __set($name, $value)
    {
        if (!property_exists($this, $name)) {
            throw new FieldNotExist($name);
        }

        if (!isset($this->$name)) {
            throw new FieldNotAllowed($name);
        }

        if (!in_array($name, $this->getFields(), true)) {
            throw new FieldNotEnabled($name);
        }

        $method = sprintf('set%s', ucfirst($name));
        if (method_exists($this, $method)) {
            $this->{$method}($value);
            return $this;
        }

        $this->$name = $value;

        return $this;
    }

    public function __isset($name)
    {
        $method = sprintf('get%s', ucfirst($name));
        if (method_exists($this, $method)) {
            return true;
        }

        return false;
    }

    protected function initFields(array $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @param bool $withoutInitValues
     * @return array
     * @internal param bool $onlyFields
     */
    public function getFields($withoutInitValues = true): array
    {
        if (!$withoutInitValues) {
            return $this->fields;
        }

        return array_keys($this->fields);
    }
}