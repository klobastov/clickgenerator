<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping;

use Cilex\Model\Click;
use Cilex\Scheme\CampaignScheme;
use Cilex\Scheme\ConditionScheme;
use Cilex\Scheme\FlowScheme;
use Cilex\Scheme\StreamScheme;

class MappedSchemes
{
    //@todo Добавить сгенерированный прокси карты, вместо ручного заполнения

    const CAMPAIGN = 100;
    const FLOW = 200;
    const STREAM = 400;
    const CONDITION = 500;
    const CLICK = 600;

    private static $import = [
        self::FLOW => FlowScheme::class,
        self::CAMPAIGN => CampaignScheme::class,
        self::STREAM => StreamScheme::class,
        self::CONDITION => ConditionScheme::class,
        self::CLICK => Click::class,
    ];

    /**
     * @return array
     */
    public static function get(): array
    {
        return self::$import;
    }

    public static function getOne(int $id)
    {
        if (!isset(self::$import[$id])) {
            return null;
        }

        return self::$import[$id];
    }

    public static function find($className): ?int
    {
        if (false !== $id = array_search($className, self::$import, true)) {
            return $id;
        }

        return null;
    }
}