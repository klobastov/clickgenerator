<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping;

use Cilex\DataMapper\DataMapperInterface;
use Cilex\DataMapper\StateTrait;
use Cilex\Scheme\Scheme;

abstract class MappedScheme extends Scheme implements DataMapperInterface
{
    use StateTrait;
}