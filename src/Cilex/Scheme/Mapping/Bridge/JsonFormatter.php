<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping\Bridge;

class JsonFormatter implements FormatterInterface
{
    protected $raw;

    public function __construct($raw)
    {
        $this->raw = $raw;
    }

    public function format(): array
    {
        return json_decode($this->raw, true);
    }
}