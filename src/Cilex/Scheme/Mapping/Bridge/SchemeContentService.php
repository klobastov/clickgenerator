<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping\Bridge;

use Cilex\Scheme\Nesting\LayerNestingScheme;

class SchemeContentService extends Service
{
    public function getContent(): array
    {
        $array = $this->implementation->format();

        /**
         * @var $current LayerNestingScheme
         */
        $current = $this->scheme::fromState([]);

        $this->trim($array, $current);

        return $array;
    }

    private function trim(array &$values, LayerNestingScheme $current): void
    {
        $fields = $current->getFields();
        $child = $this->getSingleChild($current);

        foreach ($values as $key => &$value) {
            if (false === $child) {
                break;
            }

            if (!in_array($key, $fields, true)) {
                unset($values[$key]);
                continue;
            }

            /*
             * HotFix Когда хранится коллекция, например условий\экшенов у стрима
             * @todo додумать как избавиться
             */
            if (is_array($current->$key)) {
                $value = $value[0];
            }

            if (!$this->isScheme($value, $child->getFields())) {
                continue;
            }

            $this->trim($value, $child);
        }
    }

    private function isScheme($value, array $childFields): bool
    {
        if (!is_array($value)) {
            return false;
        }

        foreach ($childFields as $field) {
            if (!isset($value[$field])) {
                return false;
            }
        }

        return true;
    }

    private function getSingleChild(LayerNestingScheme $parent)
    {
        $children = $parent->getChildren();
        if (empty($children)) {
            return false;
        }

        return array_shift($children);
    }
}