<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping\Bridge;

use Cilex\Scheme\Mapping\MappedScheme;

abstract class Service
{
    /**
     * @var MappedScheme
     */
    protected $scheme;
    protected $implementation;

    public function __construct(string $scheme, FormatterInterface $formatter)
    {
        $this->scheme = $scheme;
        $this->implementation = $formatter;
    }

    public function setImplementation(string $scheme, FormatterInterface $formatter): void
    {
        $this->scheme = $scheme;
        $this->implementation = $formatter;
    }

    abstract public function getContent(): array;
}