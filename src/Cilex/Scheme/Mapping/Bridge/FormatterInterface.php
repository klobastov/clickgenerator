<?php
declare(strict_types=1);

namespace Cilex\Scheme\Mapping\Bridge;

interface FormatterInterface
{
    public function __construct($raw);

    public function format(): array;
}