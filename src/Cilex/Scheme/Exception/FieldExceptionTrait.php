<?php
declare(strict_types=1);

namespace Cilex\Scheme\Exception;

trait FieldExceptionTrait
{
    public function __toString()
    {
        return __CLASS__ . ": [FATAL]: {$this->message}\n";
    }
}

