<?php
declare(strict_types=1);

namespace Cilex\Scheme\Exception;

use Exception;
use Throwable;

class FieldNotEnabled extends Exception
{
    use FieldExceptionTrait;

    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        $message = "Add field {$message} in array of method getFields()";
        parent::__construct($message, $code, $previous);
    }
}