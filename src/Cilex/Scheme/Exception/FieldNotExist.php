<?php
declare(strict_types=1);

namespace Cilex\Scheme\Exception;

use Exception;
use Throwable;

class FieldNotExist extends Exception
{
    use FieldExceptionTrait;

    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        $message = "Scheme not contain {$message} field";
        parent::__construct($message, $code, $previous);
    }
}