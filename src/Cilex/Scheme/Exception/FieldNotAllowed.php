<?php
declare(strict_types=1);

namespace Cilex\Scheme\Exception;

use Exception;
use Throwable;

class FieldNotAllowed extends Exception
{
    use FieldExceptionTrait;

    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        $message = "Access denied to {$message} field";
        parent::__construct($message, $code, $previous);
    }
}