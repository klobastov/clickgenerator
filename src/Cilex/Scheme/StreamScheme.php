<?php
declare(strict_types=1);

namespace Cilex\Scheme;

use Cilex\Scheme\Mapping\MappedScheme;
use Cilex\Scheme\Nesting\LayerNestingScheme;

class StreamScheme extends LayerNestingScheme
{
    public $id;
    public $conditions;

    private function __construct($id, $condition)
    {
        $this->id = $id;
        $this->conditions = $condition;
    }

    public static function fromState(array $state): MappedScheme
    {
        $object = new self(
            self::restore('id', $state),
            self::restore('conditions', $state)
        );

        $object->initFields(get_object_vars($object));

        return $object;
    }

    public function getLayer(): int
    {
        return 3;
    }
}