<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Generator\BrowserGenerator;
use Cilex\Generator\Generator;

class BrowserCondition extends Condition
{
    public function __construct($condition = null)
    {
        parent::__construct($condition);

        if (null !== $condition && !is_array($condition)) {
            throw new ConditionInvalid('Browser condition used array for options.');
        }
    }

    public function generator(): Generator
    {
        return new BrowserGenerator($this->get());
    }

    public static function fromApiState(array $state, $extra = null): array
    {
        return array_column($state, 'version', 'name');
    }
}