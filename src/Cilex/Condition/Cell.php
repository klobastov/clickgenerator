<?php
declare(strict_types=1);

namespace Cilex\Condition;

class Cell
{
    private $condition;
    private $dependencies;

    public function __construct(Condition $condition, array $dependencies = [])
    {
        $this->condition = $condition;
        $this->dependencies = $dependencies;
    }

    public function getCondition(): Condition
    {
        return $this->condition;
    }

    public function hasDependencies(): bool
    {
        return !empty($this->dependencies);
    }

    public function getDependencies(): array
    {
        return $this->dependencies;
    }
}