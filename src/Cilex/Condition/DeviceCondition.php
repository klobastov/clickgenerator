<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Generator\DeviceGenerator;
use Cilex\Generator\Generator;

class DeviceCondition extends Condition
{
    public function __construct($condition = null)
    {
        parent::__construct($condition);

        if (null !== $condition && !is_array($condition)) {
            throw new ConditionInvalid('Device condition used array for options.');
        }
    }

    public function generator(): Generator
    {
        return new DeviceGenerator($this->get());
    }

    public static function fromApiState(array $state, $extra = null): array
    {
        return array_column($state, 'brand');
    }
}