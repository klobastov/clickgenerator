<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Generator\Generator;
use Cilex\Generator\IpRangeGenerator;

class IpRangeCondition extends Condition
{
    const OPTION_RUSSIA = 'russia';
    const OPTION_TOMTEL = 'tomtel';
    const OPTION_ROSTELECOM = 'rostelecom';
    const OPTION_PATTERN = 'pattern';
    const OPTIONS_QUIET = 'without_exceptions';

    const AVAILABLE_OPTIONS = [
        self::OPTION_RUSSIA,
        self::OPTION_PATTERN
    ];

    public function __construct($condition = null)
    {
        parent::__construct($condition);

        if (null !== $condition && !is_array($condition)) {
            throw new ConditionInvalid('Ip condition used array for options.');
        }
        foreach ($condition as $item) {
            if (in_array($item, self::AVAILABLE_OPTIONS, true)) {
                throw new ConditionInvalid("Not allowed or enabled option: {$item}, in ip condition.");
            }
        }
    }

    public function generator(): Generator
    {
        return new IpRangeGenerator($this->get());
    }

    public static function fromApiState(array $state, $extra = null): array
    {
        return [];
    }
}