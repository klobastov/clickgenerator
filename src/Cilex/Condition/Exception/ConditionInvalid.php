<?php
declare(strict_types=1);

namespace Cilex\Condition\Exception;

use Exception;
use Throwable;

class ConditionInvalid extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Invalid condition';
        }
        parent::__construct($message, $code, $previous);
    }
}