<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Generator\Generator;
use Cilex\Generator\ProviderGenerator;

class ProviderCondition extends Condition
{

    public function generator(): Generator
    {
        return new ProviderGenerator($this->get());
    }

    public static function fromApiState(array $state, $extra = null): array
    {
        return $state;
    }
}