<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Condition\Exception\ConditionInvalid;
use Cilex\Generator\Generator;
use Cilex\Generator\IpGenerator;

class IpCondition extends Condition
{
    public function __construct($condition = null)
    {
        parent::__construct($condition);

        if (null !== $condition && !is_array($condition)) {
            throw new ConditionInvalid('Ip condition used array for options.');
        }
    }

    public function generator(): Generator
    {
        return new IpGenerator($this->get());
    }

    public static function fromApiState(array $state, $extra = null): array
    {
        return $state;
    }
}