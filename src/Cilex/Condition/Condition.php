<?php
declare(strict_types=1);

namespace Cilex\Condition;

use Cilex\Generator\Generator;

abstract class Condition
{
    private $condition;

    public function __construct($condition = null)
    {
        $this->condition = $condition;
    }

    public function get()
    {
        return $this->condition;
    }

    abstract public function generator(): Generator;

    //@todo можно реализовать как форметтер, независимый от condition
    abstract public static function fromApiState(array $state, $extra = null): array;
}